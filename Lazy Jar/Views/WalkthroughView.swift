//
//  WalkthroughView.swift
//  Lazy Jar
//
//  Created by Justin Anyanwu on 11/25/20.
//

import SwiftUI

struct WalkthroughView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct WalkthroughView_Previews: PreviewProvider {
    static var previews: some View {
        WalkthroughView()
    }
}
