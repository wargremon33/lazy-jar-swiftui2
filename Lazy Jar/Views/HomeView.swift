import SwiftUI

struct HomeView: View {
    
    @EnvironmentObject var authState: AuthenticationState
    
    @State var isLoading = false
    @State var error: NSError?
    let url = URL(string: "https://alfianlosari.com")!
    
    var body: some View {
        Text("Logout")
    }
    
    private func signoutTapped() {
        authState.signout()
    }
}
