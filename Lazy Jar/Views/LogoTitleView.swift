//
//  LogoTitleView.swift
//  Lazy Jar
//
//  Created by Justin Anyanwu on 11/25/20.
//

import SwiftUI

struct LogoTitleView: View {
    var body: some View {
        VStack {
            Image("logo128")
                .resizable()
                .frame(width: 100, height: 100)

            Text("Xcoding with Alfian")
                .font(.custom("SF-Pro", size: 38))
                .lineLimit(2)

            Text("Mobile Dev Articles & Tutorials")
                .font(.headline)

        }
        .foregroundColor(.white)
    }
}

struct LogoTitleView_Previews: PreviewProvider {
    static var previews: some View {
        LogoTitleView()
    }
}
