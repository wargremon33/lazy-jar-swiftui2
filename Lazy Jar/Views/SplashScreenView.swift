//
//  SplashScreenView.swift
//  Lazy Jar
//
//  Created by Justin Anyanwu on 11/25/20.
//

import SwiftUI

struct SplashScreenView: View {

    let imageName: String

    var body: some View {
        Text("Hi, this is the splash screen")
    }
}

