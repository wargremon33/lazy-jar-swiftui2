import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var authState: AuthenticationState
     
     var body: some View {
         Group {
             if authState.loggedInUser != nil {
                 HomeView()
             } else {
                 LoginView(authType: .login)
             }
         }
         .animation(.easeInOut)
         .transition(.move(edge: .bottom))
     }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
