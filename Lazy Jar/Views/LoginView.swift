//
//  LoginView.swift
//  Lazy Jar
//
//  Created by Justin Anyanwu on 11/24/20.
//

import SwiftUI

struct LoginView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var coordinator: SignInWithAppleCoordinator?
    @EnvironmentObject var authState: AuthenticationState
    @State var authType = AuthenticationType.login
    
    var body: some View {
        VStack{
            Text("Thanks for using Lazy Jar. Please login here")
            SignInAppleButton {
                self.authState.login(with: .signInWithApple)
            }
            .frame(width: 288, height: 45)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews : some View {
        LoginView()
    }
}


